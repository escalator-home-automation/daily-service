#ifndef _DAILYSERVICE_H_
#define _DAILYSERVICE_H_

#include <Arduino.h>

void set_rtc_edo(RTC_DS3231* rtc_ptr, DateTime dt_compiler_now_ido);
uint32_t get_ido_offset_seconds(DateTime dt);

class DailyService
{
  public:
  DailyService(unsigned char offset_type, float offset_hours, void (*service_fcn)(DailyService*), RTC_DS3231* rtc_ptr);
  void initServiceNextTime();
  uint32_t tomorrowServiceUnixtimeEdo(DateTime dt);

  uint32_t service_next_unix_time_edo;
 // float getOffsetHours();
  void check();
  unsigned char my_offset_type;
  float my_offset_hours;

  private:
  DateTime protectedNow();
  void (*sf)(DailyService* dsp); // = &test;
  RTC_DS3231* rtc_edo_p;
};

#endif // _DAILYSERVICE_H_
#include "RTClib.h" // https://github.com/adafruit/RTClib
#include "DailyService.h"

#define SRE_LAG_SECONDS 32 // time between comile and when the not binary actually runs on the process to set the time
                           // includes flash time and reset time
                           // https://gitlab.com/eslatt/escalator-home-automation/-/blob/master/docs/programming.md
void set_rtc_edo(RTC_DS3231* rtc_ptr, DateTime dt_compiler_now_ido)
{
 // erroneously providing ido input to function expecting edo so don't set
 // rtcs in the middle of DST start and stop nights

 // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
 // __DATE__ like 'Mar 19 2021'
 // __TIME__ like '17:01:17'
 // F is just a string clean up macro

 // https://gitlab.com/eslatt/escalator-home-automation/-/issues/13
 // https://gitlab.com/eslatt/escalator-home-automation/-/blob/master/docs/dst.md
  rtc_ptr->adjust( dt_compiler_now_ido - TimeSpan( get_ido_offset_seconds(dt_compiler_now_ido)-SRE_LAG_SECONDS));
}

uint32_t get_ido_offset_seconds(DateTime dt_edo)
{
 // ta2sim - 2 am on second Sunday in March
  DateTime ta2sim = DateTime(dt_edo.year(),3,1);
  while(ta2sim.dayOfTheWeek()!=0)
    ta2sim=ta2sim+TimeSpan(1,0,0,0);
  ta2sim=ta2sim+TimeSpan(7,2,0,0);

 // tafsin - 3 am on first Sunday in November
  DateTime tafsin = DateTime(dt_edo.year(),11,1);
  while(tafsin.dayOfTheWeek()!=0)
    tafsin=tafsin+TimeSpan(1,0,0,0);
  tafsin=tafsin+TimeSpan(0,3,0,0); // 3am because its parm is edo

  if(ta2sim<=dt_edo && dt_edo<tafsin)
    return 3600; // during the summer - DST
  return 0; // during the winter - non DST
}

DailyService::DailyService(unsigned char offset_type, float offset_hours, void (*service_fcn)(DailyService*), RTC_DS3231* rtc_ptr)
{
  my_offset_type = offset_type;
  my_offset_hours = offset_hours;
  sf = service_fcn;
  rtc_edo_p = rtc_ptr;

  initServiceNextTime();
}

DateTime DailyService::protectedNow()
{
  cli(); // disable interrupts to protect i2c
  DateTime buf=rtc_edo_p->now();
  sei(); // enable interrupts after i2c
  return buf;
}

void DailyService::initServiceNextTime()
{
  DateTime day_ago_edo = (protectedNow()-TimeSpan(1,0,0,0));
  service_next_unix_time_edo = tomorrowServiceUnixtimeEdo(day_ago_edo);
}

void DailyService::check()
{
  DateTime now_edo = protectedNow();

  if(now_edo.unixtime()>service_next_unix_time_edo)
  {
    sf(this);
    service_next_unix_time_edo = tomorrowServiceUnixtimeEdo(now_edo);
  }
}

int16_t day_of_year_idx(DateTime dt)
{
  DateTime foty = DateTime(dt.year(),1,1); // first of the year
  TimeSpan ts = dt - foty;
  return ts.days()+1;
}

const uint16_t sunrise_minutes_edo_progmem[] PROGMEM={450,450,450,450,450,450,450,450,450,450,449,449,449,448,448,448,447,447,446,446,445,445,444,443,443,442,441,440,439,439,438,437,436,435,434,433,432,431,429,428,427,426,425,424,422,421,420,418,417,416,414,413,412,410,409,407,406,404,403,401,400,398,397,395,394,392,391,389,388,386,384,383,381,379,378,376,375,373,371,370,368,366,365,363,362,360,358,357,355,353,352,350,349,347,345,344,342,341,339,337,336,334,333,331,330,328,327,325,324,322,321,319,318,317,315,314,313,311,310,309,307,306,305,304,302,301,300,299,298,297,296,295,294,293,292,291,290,289,288,287,287,286,285,284,284,283,282,282,281,281,280,280,279,279,279,278,278,278,278,277,277,277,277,277,277,277,277,277,277,277,277,278,278,278,278,279,279,279,280,280,281,281,282,282,283,283,284,284,285,286,286,287,288,288,289,290,291,291,292,293,294,295,296,296,297,298,299,300,301,302,303,304,305,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,380,381,382,383,384,385,386,387,388,389,391,392,393,394,395,396,397,398,400,401,402,403,404,405,407,408,409,410,411,412,414,415,416,417,418,419,420,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,439,440,441,442,442,443,444,444,445,446,446,447,447,448,448,448,449,449,449,449,450,450,450,450};
const uint16_t sunset_minutes_edo_progmem[] PROGMEM={1011,1012,1013,1014,1015,1015,1016,1017,1018,1019,1020,1021,1023,1024,1025,1026,1027,1028,1029,1030,1032,1033,1034,1035,1036,1038,1039,1040,1041,1042,1044,1045,1046,1047,1048,1050,1051,1052,1053,1055,1056,1057,1058,1059,1061,1062,1063,1064,1065,1066,1068,1069,1070,1071,1072,1073,1074,1076,1077,1078,1079,1080,1081,1082,1083,1084,1086,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1105,1106,1107,1108,1109,1110,1111,1112,1113,1114,1115,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1133,1134,1135,1136,1137,1138,1139,1140,1142,1143,1144,1145,1146,1147,1148,1149,1150,1151,1152,1153,1154,1154,1155,1156,1157,1158,1159,1160,1161,1162,1163,1164,1164,1165,1166,1167,1168,1169,1169,1170,1171,1171,1172,1173,1173,1174,1175,1175,1176,1176,1177,1177,1178,1178,1178,1179,1179,1179,1180,1180,1180,1180,1180,1180,1180,1181,1181,1181,1180,1180,1180,1180,1180,1180,1179,1179,1179,1178,1178,1178,1177,1177,1176,1176,1175,1174,1174,1173,1172,1172,1171,1170,1169,1168,1167,1167,1166,1165,1164,1163,1162,1160,1159,1158,1157,1156,1155,1154,1152,1151,1150,1148,1147,1146,1144,1143,1142,1140,1139,1137,1136,1135,1133,1132,1130,1129,1127,1126,1124,1122,1121,1119,1118,1116,1114,1113,1111,1110,1108,1106,1105,1103,1101,1100,1098,1096,1095,1093,1091,1090,1088,1086,1085,1083,1081,1080,1078,1076,1075,1073,1071,1070,1068,1066,1065,1063,1062,1060,1058,1057,1055,1054,1052,1051,1049,1048,1046,1045,1043,1042,1040,1039,1037,1036,1035,1033,1032,1031,1029,1028,1027,1026,1024,1023,1022,1021,1020,1019,1018,1017,1016,1015,1014,1013,1012,1011,1010,1009,1009,1008,1007,1006,1006,1005,1005,1004,1004,1003,1003,1002,1002,1002,1001,1001,1001,1001,1001,1001,1000,1000,1000,1001,1001,1001,1001,1001,1001,1002,1002,1002,1003,1003,1004,1004,1005,1005,1006,1006,1007,1008,1009,1009,1010,1010,1010,1010};


uint32_t DailyService::tomorrowServiceUnixtimeEdo(DateTime now_edo)
{
  DateTime dfn_edo = (now_edo + TimeSpan(1,0,0,0)); // day from now
  DateTime tomorrow = DateTime(dfn_edo.year(),dfn_edo.month(),dfn_edo.day());

  DateTime next_service_edo = (tomorrow + TimeSpan(my_offset_hours*3600.0f));
  if(my_offset_type == 'r') // sunrise
  {
    int16_t doy_idx = day_of_year_idx(dfn_edo);
    uint16_t offset_minutes=pgm_read_word_near(sunrise_minutes_edo_progmem+doy_idx);
    next_service_edo = next_service_edo + TimeSpan( offset_minutes*60.0f);
  }
  else if(my_offset_type == 's') // sunset
  {
    int16_t doy_idx = day_of_year_idx(dfn_edo);
    uint16_t offset_minutes=pgm_read_word_near(sunset_minutes_edo_progmem+doy_idx);
    next_service_edo = next_service_edo + TimeSpan( offset_minutes*60.0f);
  }
  else if(my_offset_type == 'a') // absolute
  {
    next_service_edo = next_service_edo - TimeSpan( get_ido_offset_seconds(dfn_edo) );
  }
  else if(my_offset_type == 't') // absolute
  {
    next_service_edo = DateTime(now_edo.year(),now_edo.month(),now_edo.day(),now_edo.hour(),now_edo.minute()+1);
  }
  return next_service_edo.unixtime();
}
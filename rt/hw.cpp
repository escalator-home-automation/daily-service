#include <stdio.h>
#include <RTClib.h>
#include <DailyService.h>

RTC_DS3231 rtc;

 // stubs three TinyWireM dependencies
void USI_TWI_Master_Stop(){printf(" stubbed USI_TWI_Master_Stop\n");}
void USI_TWI_Get_State_Info(){printf(" stubbed USI_TWI_Get_State_Info\n");}
void USI_TWI_Start_Read_Write(unsigned char*, unsigned char){printf(" stubbed USI_TWI_Start_Read_Write\n");}

uint32_t stub_unix_time;

void cli() {} // b.sh modifies interrupt.h to define this for DailyService.cpp
void sei() {} // b.sh modifies interrupt.h to define this for DailyService.cpp

DateTime RTC_DS3231::now() {
 // printf(" regression test RTC_DS3231::now stub\n");
  return DateTime(stub_unix_time);
}

void RTC_DS3231::adjust(const DateTime &dt)
{
  printf(" regression test RTC_DS3231::adjust stub\n");
  stub_unix_time = dt.unixtime();

  char buf[256];
  sprintf(buf, " // rtc.adjust(DateTime(F(\"MMM DD YYYY\"), F(\"hh:mm:ss\")));");
  printf("  %s // %d\n", dt.toString(buf), dt.unixtime()); 
}

void turn_on(DailyService* dsp)
{

}

 // see b.sh overwrite statements against /arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h
const uint16_t sunrise_minutes_progmem[] PROGMEM={300,200,100};
const uint8_t byte_progmem[] PROGMEM={30,20,10};

void test_word(int index)
{
  uint16_t looked_up = pgm_read_word_near(sunrise_minutes_progmem+index);
  printf("pgm_read_byte(byte_progmem+%d) is %d\n", index, looked_up);
}

void test_byte(int index)
{
  uint8_t looked_up = pgm_read_byte(byte_progmem+index);
  printf("pgm_read_byte(byte_progmem+%d) is %d\n", index, looked_up);
}

void test_prgmem() {
  printf("const uint16_t sunrise_minutes_progmem[] PROGMEM={300,200,100};\n");
  test_word(0);
  test_word(1);
  test_word(2);

  printf("const uint8_t byte_progmem[] PROGMEM={30,20,10};\n");
  test_byte(0);
  test_byte(1);
  test_byte(2);
}

void memcpy_P(void*a,void*b,int c)
{
  memcpy(a,b,c);
}

 // /arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h
 // #define F(string_literal) (reinterpret_cast<const __FlashStringHelper *>(PSTR(string_literal)))
#define F(string_literal) string_literal

void display_unix_time(char * indent, uint32_t unix_time)
{
  DateTime* dt = new DateTime(unix_time);
  char buf[256];
  sprintf(buf, "DDD, DD MMM YYYY hh:mm:ss");
  if(unix_time!=dt->unixtime())
  {
    printf("assert error %s %d\n", __FILE__, __LINE__);
    exit(1);
  }
  printf("%s%s (%d)\n", indent, dt->toString(buf), dt->unixtime()); 
}

void test_three_daily_service_types()
{
  DailyService* on_service;
  on_service = new DailyService('s',0,&turn_on, &rtc);
  display_unix_time("  snut_edo for 's',0 is ", on_service->service_next_unix_time_edo);  

  on_service = new DailyService('r',0,&turn_on, &rtc);
  display_unix_time("  snut_edo for 'r',0 is ", on_service->service_next_unix_time_edo);  

  on_service = new DailyService('a',20,&turn_on, &rtc);
  display_unix_time("  snut_edo for 'a',20 is ", on_service->service_next_unix_time_edo);  

  printf("\n\n\n");
}

void test_before_before()
{
  printf("test_before_after\n\n");
 // DateTime(const char *date, const char *time);
 // >Mar 19 2021<>17:01:17<
 // printf(">%s<>%s<\n", F(__DATE__), F(__TIME__));
 // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  rtc.adjust(DateTime(F("Mar 01 2021"), F("17:01:17")));
  display_unix_time("  ", rtc.now().unixtime());  
  printf("\n\n\n");

  test_three_daily_service_types();
}

void test_before_after()
{
  printf("test_before_after\n\n");
 // clock was set before dst and then add 19 days to get to 2021-03-20
  rtc.adjust(DateTime(F("Mar 01 2021"), F("17:01:17")));
  stub_unix_time+=19*24*60*60;
  display_unix_time("  ", rtc.now().unixtime());  
  printf("\n\n\n");

  test_three_daily_service_types();
}

void simulate(DateTime start_dt, TimeSpan duration_ts, DailyService*dspa[], int total_dsps)
{
  printf(" simulate ");

  for(int ds_index=0; ds_index<total_dsps; ds_index++)
    printf(" %c/%f ", dspa[ds_index]->my_offset_type, dspa[ds_index]->my_offset_hours);
  printf("\n");

  uint32_t mid_unixtime = start_dt.unixtime();
  uint32_t span_seconds = duration_ts.totalseconds();
  uint32_t start_unixtime = mid_unixtime - span_seconds/2;
  uint32_t stop_unixtime = start_unixtime + span_seconds;
  stub_unix_time = start_unixtime;

  display_unix_time("  from ", start_unixtime); 
  display_unix_time("  to ", stop_unixtime); 

  for(int ds_index=0; ds_index<total_dsps; ds_index++)
    dspa[ds_index]->initServiceNextTime();

  for(stub_unix_time = start_unixtime; stub_unix_time<stop_unixtime; stub_unix_time++)
  {
    for(int ds_index=0; ds_index<total_dsps; ds_index++)
      dspa[ds_index]->check();
  }

  printf("\n\n\n");
}

void simulate_service_event(DailyService* dsp)
{
  char buf[256];
  sprintf(buf, "  simulate_service_event %c,%f ", dsp->my_offset_type, dsp->my_offset_hours);
  display_unix_time(buf, rtc.now().unixtime());  
}

void test_get_ido_offset_seconds(DateTime dt)
{
  char buf[256];
 // DailyService* dummy = new DailyService('a',12,&simulate_service_event, &rtc);
  int ido_offset = get_ido_offset_seconds(dt);

  sprintf(buf, "  test_get_ido_offset_seconds %d ", ido_offset);
  display_unix_time(buf, dt.unixtime()); 
}

void tsre(DateTime dt_compiler_now)
{
  set_rtc_edo(&rtc, dt_compiler_now);
  printf(" tsre \n");
  display_unix_time("  compiler now ", dt_compiler_now.unixtime());
  display_unix_time("  rtc set time  ", rtc.now().unixtime());
}

void test_set_rtc_edo()
{
  printf("\n\n\n");
  tsre(DateTime(F("Mar 13 2021"), F("22:00:00"))); // 10pm Saturday right before DST start
  tsre(DateTime(F("Mar 14 2021"), F("07:00:00"))); //  7am Sunday right after DST start
  tsre(DateTime(F("Nov 06 2021"), F("22:00:00"))); // 10pm Saturday right before DST stop
  tsre(DateTime(F("Nov 07 2021"), F("07:00:00"))); //  7am Sunday right after DST stop
}

int main()
{
  printf("\n\n\n");

  test_before_before();
  test_before_after();


  test_prgmem();

  printf("\n\n\n");

  test_get_ido_offset_seconds(DateTime(F("Mar 13 2021"), F("17:59:01")));
  test_get_ido_offset_seconds(DateTime(F("Mar 14 2021"), F("01:59:59")));
  test_get_ido_offset_seconds(DateTime(F("Mar 14 2021"), F("02:00:00")));
  test_get_ido_offset_seconds(DateTime(F("Mar 14 2021"), F("17:59:01")));
  test_get_ido_offset_seconds(DateTime(F("Nov 06 2021"), F("17:59:01")));
  test_get_ido_offset_seconds(DateTime(F("Nov 07 2021"), F("02:59:59"))); // 3am because its edo
  test_get_ido_offset_seconds(DateTime(F("Nov 07 2021"), F("03:00:00"))); // 3am because its edo
  test_get_ido_offset_seconds(DateTime(F("Nov 07 2021"), F("17:59:01")));

  printf("\n\n\n");

  DailyService* dspa[] = 
  {
    new DailyService('a',12,&simulate_service_event, &rtc),
    new DailyService('s',0,&simulate_service_event, &rtc),
    new DailyService('s',0.002777778,&simulate_service_event, &rtc),
    new DailyService('r',0,&simulate_service_event, &rtc),
    new DailyService('r',-0.002777778,&simulate_service_event, &rtc)
  };
 
  printf("\n\n\n");

  simulate(DateTime(F("Mar 08 2020"), F("17:59:01")),TimeSpan(3,0,0,0), dspa, sizeof(dspa)/sizeof(dspa[0]));
  simulate(DateTime(F("Nov 01 2020"), F("17:59:01")),TimeSpan(3,0,0,0), dspa, sizeof(dspa)/sizeof(dspa[0]));
  simulate(DateTime(F("Mar 14 2021"), F("17:59:01")),TimeSpan(3,0,0,0), dspa, sizeof(dspa)/sizeof(dspa[0]));
  simulate(DateTime(F("Nov 07 2021"), F("17:59:01")),TimeSpan(3,0,0,0), dspa, sizeof(dspa)/sizeof(dspa[0]));
  simulate(DateTime(F("Dec 31 2020"), F("17:59:01")),TimeSpan(5,0,0,0), dspa, sizeof(dspa)/sizeof(dspa[0]));

  printf("\n\n\n");

  test_set_rtc_edo();

  DailyService* dspa_test_mode[] = 
  {
    new DailyService('t',0,&simulate_service_event, &rtc)
  };
  simulate(DateTime(F("Mar 08 2020"), F("17:59:01")),TimeSpan(0,0,10,0), dspa_test_mode, sizeof(dspa_test_mode)/sizeof(dspa_test_mode[0]));

  return 0;
}
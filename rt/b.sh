rm -rf ./build-x86_64-linux-gnu
mkdir ./build-x86_64-linux-gnu
mkdir ./build-x86_64-linux-gnu/libs
mkdir ./build-x86_64-linux-gnu/libs/RTClib
mkdir ./build-x86_64-linux-gnu/libs/TinyWireM
mkdir ./build-x86_64-linux-gnu/userlibs
mkdir ./build-x86_64-linux-gnu/userlibs/DailyService

sed -ie "s/# define cli()/ void cli(); \/\/ /g" /arduino-1.8.13/hardware/tools/avr/avr/include/avr/interrupt.h
sed -ie "s/# define sei()/ void sei(); \/\/ /g" /arduino-1.8.13/hardware/tools/avr/avr/include/avr/interrupt.h

sed -ie "s/#define pgm_read_byte/#define pgm_read_byte_overwritten/g" /arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h
echo '#define pgm_read_byte(addr) *((uint8_t*)addr)' >> /arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h
sed -ie "s/#define pgm_read_word_near/#define pgm_read_word_near_overwritten/g" /arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h
echo '#define pgm_read_word_near(addr) *((uint16_t*)addr)' >> /arduino-1.8.13/hardware/tools/avr/avr/include/avr/pgmspace.h

 # /arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h:#define F(string_literal) (reinterpret_cast<const __FlashStringHelper *>(PSTR(string_literal)))
sed -ie "s/#define F(/#define F_overwritten(/g" /arduino-1.8.13/hardware/arduino/avr/cores/arduino/WString.h


 # grep RTC_DS3231_overwritten_adjust /root/Arduino/libraries/RTClib/RTClib.cpp
sed -ie "s/RTC_DS3231::now/RTC_DS3231_overwritten_now/" /root/Arduino/libraries/RTClib/RTClib.cpp
sed -ie "s/RTC_DS3231::adjust/RTC_DS3231_overwritten_adjust/" /root/Arduino/libraries/RTClib/RTClib.cpp

/usr/local/bin/g++ -c \
  -DARDUINO=1813 \
  -I/arduino-1.8.13/hardware/tools/avr/avr/include \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino/api \
  -I/arduino-1.8.13/hardware/arduino/avr/variants/standard  \
  -I/root/Arduino/libraries/MillisTimer  \
  -I/root/Arduino/libraries/RTClib \
  -I/root/Arduino/libraries/TinyWireM \
  -I/arduino-1.8.13/hardware/arduino/avr/libraries/Wire/src \
  -I../src \
  -Wall -ffunction-sections -fdata-sections -Os \
  -fpermissive -fno-exceptions -std=gnu++11 -fno-threadsafe-statics -flto \
  -fno-devirtualize -fdiagnostics-color=always /root/Arduino/libraries/RTClib/RTClib.cpp \
  -o ./build-x86_64-linux-gnu/libs/RTClib/RTClib.cpp.o

/usr/local/bin/g++ -c \
  -DARDUINO=1813 \
  -I/arduino-1.8.13/hardware/tools/avr/avr/include \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino/api \
  -I/arduino-1.8.13/hardware/arduino/avr/variants/standard  \
  -I/root/Arduino/libraries/MillisTimer  \
  -I/root/Arduino/libraries/RTClib \
  -I/root/Arduino/libraries/TinyWireM \
  -I/arduino-1.8.13/hardware/arduino/avr/libraries/Wire/src \
  -I../src \
  -Wall -ffunction-sections -fdata-sections -Os \
  -fpermissive -fno-exceptions -std=gnu++11 -fno-threadsafe-statics -flto \
  -fno-devirtualize -fdiagnostics-color=always /root/Arduino/libraries/TinyWireM/TinyWireM.cpp \
  -o ./build-x86_64-linux-gnu/libs/TinyWireM/TinyWireM.cpp.o

/usr/local/bin/g++ -c \
  -DARDUINO=1813 \
  -I/arduino-1.8.13/hardware/tools/avr/avr/include \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino/api \
  -I/arduino-1.8.13/hardware/arduino/avr/variants/standard  \
  -I/root/Arduino/libraries/MillisTimer  \
  -I/root/Arduino/libraries/RTClib \
  -I/arduino-1.8.13/hardware/arduino/avr/libraries/Wire/src \
  -I../src \
  -Wall -ffunction-sections -fdata-sections -Os \
  -fpermissive -fno-exceptions -std=gnu++11 -fno-threadsafe-statics -flto \
  -fno-devirtualize -fdiagnostics-color=always ../src/DailyService.cpp \
  -o ./build-x86_64-linux-gnu/userlibs/DailyService/DailyService.cpp.o

/usr/local/bin/g++ -c \
  -DARDUINO=1813 \
  -I/arduino-1.8.13/hardware/tools/avr/avr/include \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino \
  -I/arduino-1.8.13/hardware/arduino/avr/cores/arduino/api \
  -I/arduino-1.8.13/hardware/arduino/avr/variants/standard  \
  -I/root/Arduino/libraries/MillisTimer  \
  -I/root/Arduino/libraries/RTClib \
  -I/arduino-1.8.13/hardware/arduino/avr/libraries/Wire/src \
  -I../src \
  -Wall -ffunction-sections -fdata-sections -Os \
  -fpermissive -fno-exceptions -std=gnu++11 -fno-threadsafe-statics -flto \
  -fno-devirtualize -fdiagnostics-color=always hw.cpp \
  -o ./build-x86_64-linux-gnu/hw.cpp.o

g++ -o ./build-x86_64-linux-gnu/hw.out \
  ./build-x86_64-linux-gnu/hw.cpp.o \
  ./build-x86_64-linux-gnu/userlibs/DailyService/DailyService.cpp.o \
  ./build-x86_64-linux-gnu/libs/RTClib/RTClib.cpp.o \
  ./build-x86_64-linux-gnu/libs/TinyWireM/TinyWireM.cpp.o

echo to-do - remove the requirement for sed against RTClib.cpp

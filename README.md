# Daily Service

## Overview

This library was developed with the [Escalator Switch](https://gitlab.com/escalator-home-automation/escalator-switch), which combines a power supply, non-invasive binary current sensor, over-center relay, microcontroller and real-time clock in a package that is screwed to the back of a 3-way light switch.  Daily Service allows a function to be triggered daily based on absolute time or relative time based on sunsite or sunrise.

## References

* [Escalator Switch Project](https://gitlab.com/escalator-home-automation/escalator-switch)
* [Original Escalator Home Automation Project](https://gitlab.com/eslatt/escalator-home-automation)